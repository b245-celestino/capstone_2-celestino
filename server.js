const { application } = require("express");
const express = require("express");
const { default: mongoose } = require("mongoose");
const cors = require("cors");
const port = 4000;
const app = express();
const userRoutes = require("./routes/userRoutes");
const productsRoutes = require("./routes/productsRoutes");
const ordersRoutes = require("./routes/ordersRoutes");

mongoose.set("strictQuery", true);
mongoose.connect(
  "mongodb+srv://admin:admin@batch245-celestino.ldu2t4p.mongodb.net/Capstone_2_Celestino?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection Error"));

db.once("open", () => {
  console.log("We are now connected to the cloud");
});

// Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use("/user", userRoutes);
app.use("/product", productsRoutes);
app.use("/order", ordersRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}!`));
