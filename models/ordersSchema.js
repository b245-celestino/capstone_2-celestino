const mongoose = require("mongoose");

const ordersSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  products: [
    {
      productId: {
        type: String,
        required: true,
      },
      name: {
        type: String,
        required: true,
      },
      quantity: {
        type: Number,
        default: 1,
      },
      price: {
        type: Number,
        required: true,
      },
      subtotal: {
        type: Number,
      },
    },
  ],
  totalAmount: {
    type: Number,
    default: 0,
  },
  orderStatus: {
    type: String,
    default: "completed",
  },
  transactionDate: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model("Order", ordersSchema);
