const mongoose = require("mongoose");

const productsSchema = new mongoose.Schema({
  image: {
    type: String,
    required: [true, "Image is required!"],
  },

  name: {
    type: String,
    required: [true, "Name is required!"],
  },
  description: {
    type: String,
    required: [true, "Description is required!"],
  },
  price: {
    type: Number,
    required: [true, "Price is required!"],
  },
  quantity: {
    type: Number,
    required: true,
    default: 0,
  },
  isAvailable: {
    type: Boolean,
    default: true,
  },
  createdOn: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model("Products", productsSchema);
