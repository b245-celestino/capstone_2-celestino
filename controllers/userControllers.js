const mongoose = require("mongoose");
const User = require("../models/usersSchema");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const { request } = require("express");
const Product = require("../models/productsSchema");
const Order = require("../models/ordersSchema");

const usersSchema = require("../models/usersSchema");

// User REGISTRATION
module.exports.userRegistration = async (request, response) => {
  const input = request.body;
  await User.findOne({ email: input.email })
    .then((result) => {
      if (result !== null) {
        return response.send(false);
      } else {
        let newUser = new User({
          firstName: input.firstName,
          lastName: input.lastName,
          email: input.email,
          password: bcrypt.hashSync(input.password, 10),
        });

        newUser
          .save()
          .then((save) => {
            return response.send(true);
          })
          .catch((error) => {
            return response.send(error);
          });
      }
    })
    .catch((error) => {
      return response.send(false);
    });
};

// USER AUTHENTICATION

module.exports.userAuthentication = (request, response) => {
  let input = request.body;

  //Possible scenarios in logging in
  //1.email is not yet registered
  //2. email is registered but the password is wrong

  User.findOne({ email: input.email })
    .then((result) => {
      if (result === null) {
        return response.send(true);
      } else {
        // "compareSync()" method used to compare a non encrypted password to the encrypted password.
        // it returns boolean value, if match true value will return otherwise false.
        const isPasswordCorrect = bcrypt.compareSync(
          input.password,
          result.password
        );

        if (isPasswordCorrect) {
          return response.send({
            auth: auth.createAccessToken(result),
          });
        } else {
          return response.send(false);
        }
      }
    })
    .catch((error) => {
      return response.send(error);
    });
};

// RETRIEVE USER DETAILS

module.exports.getProfile = (request, response) => {
  // let input = request.body;
  const userData = auth.decode(request.headers.authorization);

  // console.log(userData);

  User.findById(userData._id)
    .then((result) => {
      // result.password = "";
      return response.send(result);
    })
    .catch((error) => {
      return response.send(error);
    });
};

module.exports.addToCart = async (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  let productId = request.params.productId;
  let input = request.body;
  if (userData.isAdmin === true) {
    return response.send("Admins are not allowed to create an order");
  }
  await Product.findById(productId).then((result) => {
    const product = {
      productName: result.name,
      quantity: input.quantity,
      totalAmount: result.price * input.quantity,
    };
    console.log(product);
    User.findById(userData._id).then((result) => {
      result.cart.push(product);
      return result.save().then((save) => {
        return response.send(
          "Your product has successfully added in your cart!"
        );
      });
    });
  });
};

module.exports.setAdmin = (request, response) => {
  let idToBeRetrieve = request.params.id;

  User.findByIdAndUpdate(
    idToBeRetrieve,
    {
      isAdmin: true,
    },
    { new: true }
  )
    .then((result) => {
      const adminData = auth.decode(request.headers.authorization);
      if (adminData.isAdmin == true) {
        result.password = "";
        return response.send(result);
      } else {
        return response.send("You are not an admin");
      }
    })
    .catch((error) => {
      return response.send(error);
    });
};
