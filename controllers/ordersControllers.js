const mongoose = require("mongoose");
const Order = require("../models/ordersSchema");
const Product = require("../models/productsSchema");
const User = require("../models/usersSchema");
const auth = require("../auth.js");

// add items in cart
module.exports.createOrder = async (request, response) => {
  try {
    const userData = auth.decode(request.headers.authorization);

    if (userData.isAdmin === true) {
      return response.send("Admins are not allowed to create an order");
    }
    await Order.findOne({ userId: userData._id, orderStatus: "pending" }).exec(
      (error, order) => {
        if (error) return response.status(400).json({ error });

        if (order) {
          // if order already exist
          const productId = request.body.products.productId;
          const item = order.products.find(findProductId);
          function findProductId(Id) {
            return Id.productId == productId;
          }
          console.log(order);

          if (item) {
            Order.findOneAndUpdate(
              {
                userId: userData._id,
                "products.productId": productId,
                orderStatus: "pending",
              },

              {
                $set: {
                  "products.$": {
                    ...request.body.products,
                    quantity: item.quantity + request.body.products.quantity,
                    subTotal:
                      item.price *
                      (item.quantity + request.body.products.quantity),
                  },
                },
              },
              { new: true }
            )

              .then((result) => {
                const sub_total = order.products.reduce((sum, sub) => {
                  return sum + sub.subTotal;
                }, 0);

                result.totalAmount =
                  sub_total +
                  request.body.products.quantity * request.body.products.price;
                result.save();
                return response.send(result);
              })
              .catch((error) => {
                return response.send(error);
              });
          } else {
            Order.findOneAndUpdate(
              { userId: userData._id, orderStatus: "pending" },

              {
                $push: {
                  products: request.body.products,
                  products: {
                    ...request.body.products,
                    subTotal:
                      request.body.products.price *
                      request.body.products.quantity,
                  },
                },
              },

              { new: true }
            ).exec((error, result) => {
              if (error) return response.status(400).json({ error });
              if (result) {
                const sub_total = result.products.reduce((sum, sub) => {
                  return sum + sub.subTotal;
                }, 0);

                result.totalAmount = sub_total;
                result.save();
                return response.send(result);
              }
            });
          }
        } else {
          const subtotal =
            request.body.products.price * request.body.products.quantity;
          //  if cart not EXIST
          const order = new Order({
            userId: userData._id,
            products: request.body.products,
            products: {
              ...request.body.products,
              subTotal:
                request.body.products.price * request.body.products.quantity,
            },
            totalAmount: subtotal,
          });

          order.save((error, order) => {
            if (error) return response.status(400).json({ error });
            if (order) {
              return response.send(order);
            }
          });
        }
      }
    );
  } catch (error) {
    return response.send({ message: "There was an error. Please try again!" });
  }
};

// check all orders

module.exports.allOrders = async (request, response) => {
  try {
    const userData = auth.decode(request.headers.authorization);

    if (!userData.isAdmin) {
      return response.send("You're not authorized!");
    }
    const order = await Order.findOne({ orderStatus: "completed" }).exec();

    return response.send(order);
  } catch (error) {
    return response.send({ message: "There was an error. Please try again!" });
  }
};

// retrieve user order
module.exports.specificOrder = async (request, response) => {
  try {
    // to get the params from the url
    const userData = auth.decode(request.headers.authorization);

    if (userData.isAdmin) {
      return response.send(false);
    }
    const order = await Order.find({
      userId: userData._id,
      // orderStatus: "completed",
    }).exec();
    if (!order || order == "") {
      return response.send("Cart not found!");
    }

    if (order) {
      return response.send(order);
    }
    return response.send(order);
  } catch (error) {
    return response.send({ message: "There was an error. Please try again!" });
  }
};

// Updating quantity of products in the order

exports.changeQuantity = async (request, response) => {
  try {
    const userData = auth.decode(request.headers.authorization);
    if (userData.isAdmin) {
      return response.send("You are not allowed to access this page!");
    }
    const order = await Order.findOne({
      userId: userData._id,
      orderStatus: "pending",
    }).exec();
    if (!order) {
      return response.send("Cart not found!");
    }

    const itemToRemove = request.params.productId;
    const item = await Product.findOne({ _id: itemToRemove }).exec();

    if (!item) {
      return response.send("Invalid product ID!");
    }
    let itemIndex = null;

    const orderItem = order.products.find((i, index) => {
      if (i.productId === itemToRemove) {
        itemIndex = index;
        return true;
      }
      return false;
    });

    if (!orderItem) {
      return response.send(
        "Item is not in your cart. Please add the item first!"
      );
    }

    if (request.body.products.quantity <= 0) {
      order.products.splice(itemIndex, 1);
      return response.send(await order.save());
    }

    orderItem.quantity = request.body.products.quantity;

    orderItem.subTotal = orderItem.quantity * orderItem.price;

    const sub_total = order.products.reduce((sum, sub) => {
      return sum + sub.subTotal;
    }, 0);

    order.totalAmount = sub_total;

    const orderSave = await order.save();

    return response.send(orderSave);
  } catch (error) {
    return response.send({ message: "There was an error. Please try again!" });
  }
};

// Removing items in the cart

exports.removeItemsInCart = async (request, response) => {
  try {
    const userData = auth.decode(request.headers.authorization);
    if (userData.isAdmin) {
      return response.send("You are not allowed to access this page!");
    }
    const order = await Order.findOne({
      userId: userData._id,
      orderStatus: "pending",
    }).exec();
    if (!order) {
      return response.send("Cart not found!");
    }

    const itemToRemove = request.params.productId;
    const item = await Product.findOne({ _id: itemToRemove }).exec();

    if (!item) {
      return response.send("Invalid product ID!");
    }
    let itemIndex = null;

    const orderItem = order.products.find((i, index) => {
      if (i.productId === itemToRemove) {
        itemIndex = index;
        return true;
      }
      return false;
    });
    console.log(orderItem);
    order.totalAmount = order.totalAmount - orderItem.subTotal;
    order.products.splice(itemIndex, 1);

    const orderSave = await order.save();

    return response.send(orderSave);
  } catch (error) {
    return response.send({
      message:
        "There was an error! Please check product ID or the product is not yet added in your cart!",
    });
  }
};

module.exports.sampleOrder = async (request, response) => {
  const input = request.body;

  const userData = auth.decode(request.headers.authorization);

  if (userData.isAdmin === false) {
    await Order.findOne({ userId: userData._id }).then((result) => {
      console.log(result);
      if (result !== null) {
        let newOrder = new Order({
          userId: userData._id,
          products: input.products,
          totalAmount: input.totalAmount,
        });

        return newOrder
          .save()
          .then((save) => {
            return response.send(true);
          })
          .catch((error) => {
            return response.send(error);
          });
      } else {
        let newOrder = new Order({
          userId: userData._id,
          products: input.products,
          totalAmount: input.totalAmount,
        });

        return newOrder
          .save()
          .then((save) => {
            return response.send(true);
          })
          .catch((error) => {
            return response.send(error);
          });
      }
    });
  } else {
    return false;
  }
};

//  Order Checkout

exports.orderCheckout = async (request, response) => {
  try {
    const userData = auth.decode(request.headers.authorization);

    if (userData.isAdmin) {
      return response.send(false);
    }

    const order = await Order.findOne({
      userId: userData._id,
    }).exec();
    if (!order) {
      return response.send(false);
    }

    if (order.products === undefined || order.products.length === 0) {
      return response.send(false);
    }
    order.orderStatus = "completed";
    order.transactionDate = new Date();
    const orderSave = await order.save();

    let newOrder = new Order({ userId: userData._id });

    await newOrder.save();

    return response.send(orderSave);
  } catch (error) {
    return response.send({ message: "There was an error. Please try again!" });
  }
};

// View user cart

exports.viewCart = async (request, response) => {
  try {
    const userData = auth.decode(request.headers.authorization);

    if (userData.isAdmin) {
      return response.send("You're not authorized!");
    }
    const order = await Order.find({
      userId: userData._id,
      orderStatus: "pending",
    }).exec();

    if (!order || order == "") {
      return response.send("Cart not found!");
    }
    return response.send(order);
  } catch (error) {
    return response.send({ message: "There was an error. Please try again!" });
  }
};
