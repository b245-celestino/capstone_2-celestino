const express = require("express");
const router = express.Router();
const productsControllers = require("../controllers/productsControllers");
const auth = require("../auth");

//Route for creating a product
router.post("/add", auth.verify, productsControllers.addProduct);

router.get("/allproducts", productsControllers.retrieveAllProducts);

router.get("/:id", productsControllers.retrieveSingleProduct);

router.put(
  "/:id/updateproduct",
  auth.verify,
  productsControllers.updateProduct
);
router.put(
  "/:id/archiveproduct",
  auth.verify,
  productsControllers.archiveProduct
);

router.put(
  "/:id/activateproduct",
  auth.verify,
  productsControllers.activateProduct
);

module.exports = router;
