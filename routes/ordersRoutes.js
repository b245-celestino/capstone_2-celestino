const express = require("express");
const router = express.Router();
const ordersControllers = require("../controllers/ordersControllers");
const auth = require("../auth");

router.post("/checkout", auth.verify, ordersControllers.createOrder);

router.post("/sample", auth.verify, ordersControllers.sampleOrder);

router.get("/all", auth.verify, ordersControllers.allOrders);

router.get("/viewCart", auth.verify, ordersControllers.viewCart);

router.put("/checkout", auth.verify, ordersControllers.orderCheckout);

router.get("/retrieve", auth.verify, ordersControllers.specificOrder);

router.put(
  "/remove/:productId",
  auth.verify,
  ordersControllers.removeItemsInCart
);

router.post(
  "/quantity/:productId",
  auth.verify,
  ordersControllers.changeQuantity
);

module.exports = router;
