const express = require("express");
const router = express.Router();
const auth = require("../auth");

const userController = require("../controllers/userControllers");
// Routes
// User REGISTRATION
router.post("/register", userController.userRegistration);
// USER AUTHENTICATION
router.post("/login", userController.userAuthentication);

router.get("/details", auth.verify, userController.getProfile);

router.patch("/admin/set/:id", auth.verify, userController.setAdmin);

router.post("/addCart/:productId", auth.verify, userController.addToCart);
module.exports = router;
